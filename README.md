# TR600-MX287


此网页用于记录项目遇到的问题，产品测试方法，产品版本控制

遇到的问题向上面提，点击 **工单管理（ISSUES）**，发布帖子，解决方案会更在帖子后面

# 软件版本

  - [uboot 修改概要](http://g-link:3000/GlinkLib/uboot-mx287/src/master/ChangeLog.md)

  - [linux 修改概要](./doc/changelog-linux.md)

  - [文件系统 修改概要](http://g-link:3000/GlinkLib/rootfs-mx287)，包括如下
	  - otdr-base FPGA通信程序
	  - mx287_feature_demo 乱七八糟小程序
    - TR700 OTDR界面
    - libtr600plus-algo.so  算法库  

  - [otdr-base 修改概要](http://g-link:3000/GlinkLib/otdr-base/src/master/ChangeLog.md)
  	- [各版本细节](http://g-link:3000/GlinkLib/otdr-base)

  - [mx287_feature_demo 修改概要](http://g-link:3000/GlinkLib/mx287_feature_demo/src/master/ChangeLog.md)


  - [电路板详细](./doc/board-detail.md)
   
## 目录组成结构
- [defect](./defect)  
已知缺陷
- [download](./download)  
下载程序，包括描述程序版本兼容性
- [pcb-test](./pcb-test)  
如何测试电路板
- [issues](./issues)  
部分重要讨论内容

- [feature](./feature)  
技术特性

