U盘/SD卡升级系统方法V1.2
==========================

# 1 可升级内容

  可选择如下任意1个或多个内容升级

  - **logo.bmp**  
  	启动LOGO，支持16bit、8bit深度BMP格式
  - **uImage**  
  	操作系统
  - **update.tar**  
  	文件系统升级补丁
  - **autorun.sh**  
    非升级内容，用于升级以外的操作，比如导出日志、设备当前测试报告等，目前未对该脚本限制权限，对系统安全极不安全，未来会有限制。

# 2 升级演示
  将所有文件放入移动设备的update目录下

![][U盘内容]

  弹出窗口后按Yes执行升级，__整个过程切勿断电__，否则No，10秒无操作窗口自动关闭

![][升级提示]

  升级结束5秒后窗口自动关闭，重启设备测试升级效果
  
![][升级过程]
## 2.1 注意
------------

> 原则上不允许在升级过程拔下存储器、掉电，有变砖风险，变砖后只能通过SD卡恢复，恢复方法另外述。

  - logo升级掉电失败，启动logo变成企鹅
  - uImage升级掉电失败
    - 如果烧录的uboot版本是imx28_ivt_uboot-v1.1-rc2.sb，设备变砖
    - 如果烧录的uboot版本是imx28_ivt_uboot-v1.2-rc1.sb，设备可正常启动，设备具有双内核，第一内核错误后可从第二内核启动，此时可重新插入U盘升级第一内核，无法从界面上看出系统是从哪个内核启动的，__如果是从第二内核启动那么他的启动时间将长2-3秒__ 。第二内核是出厂时的内容，永远不修改，也就是第二内核如果有任何BUG都不会被修复。
  - update.tar升级掉电失败，根据升级内容的重要性有一定几率变砖， __原则上研发不会对出厂产品提供update.tar内包含可能导致系统变砖的升级包__




# 3 前缀命名规则
  老版本的设备只支持固定命名格式，新设备可识别文件前缀，只要文件名满足下面三条规则

  - updateXXX.tar
  - logoXXX.bmp
  - uImageXXX

![][命名规则]

## 3.1 多补丁同时升级
  上图中存储设备里同时存在三个升级包，他们将按照__名字__的先后顺序被升级

  - update-080410-fix-mmc.tar
  - update-080522-fix-log.tar
  - update-080523-language-en.tar

## 3.2 注意
>  研发人员提供升级包时必须严格按照命名规则： update-时间-描述.tar 


## 3.3 设备名前缀升级包
  因为所有型号都采用相同的 __默认命名规则__，所以可能误升级其他设备的补丁
  （如TR600的补丁给TF500），所以升级文件还支持设备名前缀。
  TR600的前缀是 __TR600-Plus[L3.3]__

![][多补丁同时升级]  
  
  前缀配置保存在  __/sys/class/misc/product/board __

```
[root@TR600-Plus[L3 /]# cat /sys/class/misc/product/board 
TR600-Plus[L3.3]
```
  
  在升级过程也能看到前缀

![][Qt显示前缀]





# 4 问答
----
__问__  当同时存在
  update-1.tar、update-2.tar、logo-1.bmp、logo-2.bmp，它们全部都被升级吗？

__答__ update-1.tar、update-2.tar按照名字顺序先后升级，
  logo-1.bmp、logo-2.bmp只有__名字靠前__的唯一一个被升级，同理uImage的升级逻辑也与
  logo相同

  ![][多LOGO如何升级]

----

__问__  当同时存在
  TR600-Plus[L3.3]-uImage-v1.2、uImage-v1.2 将升级谁?

__答__  顺序先写入TR600-Plus[L3.3]-uImage-v1.2之后被uImage-v1.2重新覆盖

---

__问__  多个升级包如何保证他们之间互补冲突？会不会导致版本回退?

__答__  完全有可能冲突与版本回退，但是应该避免。这些由研发人员保障，必要时开发人员告诉中式哪些升级包的作用范围，哪些升级包可删除。

按规则：

  - 不同开发人员之间提供的升级包 __应允许不分先后顺序的升级__
  - 所有开发人员提供的升级包应该__仅包__含自己的文件，不胡乱修改他人的
  - 开发人员的升级包养成按照__时间命名规则__，如：update-080101-fix-A.tar、update-080103-fix-B.tar，这样保证按照名字先后顺序被升级，避免升级包之间包含相同内容而导致版本回退
  

---

__问__  升级包描述有什么用

__答__  没有任何描述的升级包也可以用，但如果有描述便于管理，看名字就知道，
同时设备内部会记录所有升级的内容，版本可追寻、可定位升级引起的故障。

```
[root@TR600-Plus[L3 /]# tail /var/log/messages 
Jun 14 15:14:27 (none) user.notice root: update rootfs /mnt/udisk/update/TR600-Plus[L3.3]-update-080410-fix-mmc.tar /mnt/udisk/update/TR600-Plus[L3.3]-update-080522-fix-log.tar /mnt/udisk/update/TR600-Plus[L3.3]-update-080523-language-en.tar
Jun 14 15:16:39 (none) user.notice root: update Kernel /mnt/udisk/update/uImage-v1.2-beta3
Jun 14 15:16:41 (none) user.notice root: update LOGO /mnt/udisk/update/logo-Glink.bmp
Jun 14 15:16:42 (none) user.notice root: update rootfs /mnt/udisk/update/update-080410-fix-mmc.tar /mnt/udisk/update/update-080522-fix-log.tar /mnt/udisk/update/update-080523-language-en.tar
Jun 14 15:18:04 (none) user.notice root: update Kernel /mnt/udisk/update/uImage-v1.2-beta3
Jun 14 15:18:06 (none) user.notice root: update LOGO /mnt/udisk/update/logo-Glink.bmp
Jun 14 15:18:07 (none) user.notice root: update rootfs /mnt/udisk/update/update-080522-fix-mmc.tar
```

---

__问__  升级包能以中文命名吗？如 logo-中性.bmp
  
__答__ 不能，可能会引起设备数据记录混乱，请用拼音字母代替


---

__问__  为什么我的当前的设备不支持上文所描述的升级包前缀和多补丁同时升级?
  
__答__ 很可能是当前的设备是老版本，没有如上功能，先下载，将文件重命名为update.tar，
拷贝到存储器的update目录下，按照上面的教程操作。

  - [tr600 下载点击这里][tr600补丁]
  - [tr700 下载点击这里][tr700补丁]
---

__问__ 为什么当前设备不存在 __/sys/class/misc/product/board__ ，界面上不显示 
__"Device TR600-Plus[L3.3]"__ 而是 __"Device "__

__答__ 只有 uImage-v1.2-beat3 及以上的内核才支持

__问__ 为什么存在 __/sys/class/misc/product/board__ ，但内容是 __(null)__ 或 __空__

__答__ 只有 uboot是 v1.2-rc1 版本以上常能最好支持，uboot不能后期升级

---
[U盘内容]:./images/udisk.png
[升级提示]:./images/update-new-1.png
[升级过程]:./images/update-new-2.png
[命名规则]:./images/update-3.png
[多补丁同时升级]:./images/update-4.png
[Qt显示前缀]:./images/update-prefix.png
[多LOGO如何升级]:./images/update-logo-2.png


# 5 附件
  - [tr600 支持升级包前缀和多补丁同时升级][tr600补丁]
  - [tr600 支持升级包前缀和多补丁同时升级][tr700补丁]

[tr600补丁]: ./update-180622-v1.2-tr600.tar
[tr700补丁]: ./update-180622-v1.2-tr700.tar