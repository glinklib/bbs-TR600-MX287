# 测试：RTC使用与测试
> 2016-10-12 MenglongWu

## 1. MX287内部RTC

核心板内部时钟的电源口并没有从开发板上引出，不能再断电情况下通过纽扣电池给其供电。具体硬件连接方法参考Freescale官方公板原理图《IMX28EVK_revD》

![image](images/mxs-bat-pad.png)

## 2. PCF8563外部RTC

《EPC-28AI-L-T原理图_V1.00》所示，时钟闹铃中断未连接到任何端口，如果需要启用闹铃功能需要自行连接到mx287外部中断。
![image](images/PCF8563-INT.png)


### 2.1 信号
纽扣电池供电3V  
万用表测量PCF8563的晶振输入管脚之间电压 **310mV**  
示波器CH1、CH2测量晶振信号

项目	                |    值	
---                     |---        
CH1-Vage 	            |    18mV	
CH2-Vage 	            |    387mV	
Amplitude               |    262mV
Frequency	            |    32.025KHz
CH1-Vage - CH2-Vage 	|    369mV

![image](images/osc.png)

## 2.2 软件测试

### 正常状态


* 获取时钟

```
[root@M28x nfs]# hwclock  -r
[   86.920000] rtc-pcf8563 1-0051: low voltage detected, date/time is not reliable.
Fri Oct 14 12:52:22 2016  0.000000 seconds
```

- 探测I2C-1总线设备

```
[root@M28x nfs]# ./i2cdetect -y 1
     0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f
00:          -- -- -- -- -- -- -- -- -- -- -- -- -- 
10: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
20: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
30: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
40: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
50: -- UU -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
60: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
70: -- -- -- -- -- -- -- --   
```

- 写入内容

```
[root@M28x nfs]# ./i2cdump -f -y 1 0x51
No size specified (using byte-data access)
     0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f    0123456789abcdef
00: 08 c0 d1 d1 d2 d4 d5 d0 16 e1 f3 c1 c1 d0 53 2f    ??????????????S/
10: 08 c0 d1 d1 d2 d4 d5 d0 16 e1 f3 c1 c1 d0 53 2f    ??????????????S/
20: 08 c0 d1 d1 d2 d4 d5 d0 16 e1 f3 c1 c1 d0 53 2f    ??????????????S/
30: 08 c0 d1 d1 d2 d4 d5 d0 16 e1 f3 c1 c1 d0 53 2f    ??????????????S/
40: 08 c0 d1 d1 d2 d4 d5 d0 16 e1 f3 c1 c1 d0 53 2f    ??????????????S/
50: 08 c0 d1 d1 d2 d4 d5 d0 16 e1 f3 c1 c1 d0 53 2f    ??????????????S/
60: 08 c0 d1 d1 d2 d4 d5 d0 16 e1 f3 c1 c1 d0 53 2f    ??????????????S/
70: 08 c0 d1 d1 d2 d4 d5 d0 16 e1 f3 c1 c1 d0 53 2f    ??????????????S/
80: 08 c0 d1 d1 d2 d4 d5 d0 16 e1 f3 c1 c1 d0 53 2f    ??????????????S/
90: 08 c0 d1 d1 d2 d4 d5 d0 16 e1 f3 c1 c1 d0 53 2f    ??????????????S/
a0: 08 c0 d1 d1 d2 d4 d5 d0 16 e1 f3 c1 c1 d0 53 2f    ??????????????S/
b0: 08 c0 d1 d1 d2 d4 d5 d0 16 e1 f3 c1 c1 d0 53 2f    ??????????????S/
c0: 08 c0 d1 d1 d2 d4 d5 d0 16 e1 f3 c1 c1 d0 53 2f    ??????????????S/
d0: 08 c0 d1 d1 d2 d4 d5 d0 16 e1 f3 c1 c1 d0 53 2f    ??????????????S/
e0: 08 c0 d1 d1 d2 d4 d5 d0 16 e1 f3 c1 c1 d0 53 2f    ??????????????S/
f0: 08 c0 d1 d1 d2 d4 d5 d0 16 e1 f3 c1 c1 d0 53 2f    ??????????????S/
```


### 异常情况

如下情况用于生产环境定位硬件故障点。

- 短路晶振
	- 获取时间失败，原因是i2c_transfer发送超时
	- 总线能穷举

```
[root@M28x nfs]# hwclock  -r
[ 1351.350000] rtc-pcf8563 1-0051: pcf8563_get_datetime: read error
hwclock: RTC_RD_TIME: Input/output error
```

- GND短接I2C-SCL
	- 获取时间失败
	- 总线无法穷举，速度很慢，

```
[root@M28x ~]# hwclock  -r
rtc-pcf8563 1-0051: pcf8563_get_datetime: read error
hwclock: RTC_RD_TIME: Input/output error
```

### 注意
当短接I2C-SCL时，总线通讯永远超时，扫描情况会变得很慢， **所以** ，当应用程序读取I2C总线时异常变慢请检查I2C总线电路。


- GND短接I2C-SDA
	- 穷举出无效设备
	- 获取时间全0

```
[root@M28x nfs]# hwclock -r
[  780.020000] rtc-pcf8563 1-0051: retrieved date/time is not valid.
Tue Nov 30 00:00:00 1999  0.000000 seconds

[root@M28x nfs]# ./i2cdetect  -y 1
     0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f
00:          -- -- -- -- -- -- -- -- -- -- -- -- -- 
10: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
20: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
30: 30 31 32 33 34 35 36 37 -- -- -- -- -- -- -- -- 
40: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
50: 50 UU 52 53 54 55 56 57 58 59 5a 5b 5c 5d 5e 5f 
60: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
70: -- -- -- -- -- -- -- -- 


[root@M28x nfs]# ./i2cdump  -f -y 1 0x51
No size specified (using byte-data access)
     0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f    0123456789abcdef
00: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00    ................
10: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00    ................
20: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00    ................
```




