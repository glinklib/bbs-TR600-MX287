# 测试：验证SPI是否正常
/app/fpga-2016-chip.elf -d 1 -d r

### 参数意义
- -d 1 打开调试
- -d r 显示接收到的内容

更多使用参数查阅 
https://git.oschina.net/glinklib/otdr-base
（工具使用 - otdr 硬件功能完整性测试工具）

## 正常
发送两条命令：

- 询问spi是否正常
- 检测通信光

![images](./images/otdr-fpga-spi-ok.png)
## 异常
在询问spi是否正常时没有响应，反复测试，多次测试都没有响应就退出

![images](./images/otdr-fpga-spi-test.png)

## FPGA-SPI死循环测试

用于开机自动运行（在没有网络的情况下）与FPGA通信，测试SPI是否短路（需要示波器）
** /app/fpga-2016-chip.elf -d 1 -d r -F ** 
末尾加入 **-F** 无论FPGA是否存在都反复发送 **询问spi是否正常命令** 

### FPGA-SPI缺陷
FPGA 的 spi 通信有小问题，不是每次命令都能正常解析，连续发送相同的命令，也会丢失，**好在** 丢失都 **只有一次**，ARM端通过多次发送解决

![images](./images/otdr-fpga-spi-forever.png)

