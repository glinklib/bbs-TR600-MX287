# 测试：开机启动并发送SPI数据

该需求是不是发布版本该有的，需要在烧录的 **标准rootfs** 环境下作如下修改

## 修改/etc/init.d/rcS

- **原始内容**

```
#/app/fpga-2016-chip.elf -F &
#/app/qt.elf -qws &
exit
```

- **修改内容**

```
/app/fpga-2016-chip.elf -F &
#/app/qt.elf -qws &
exit
```

- **保存并重启**

```
~ # sync
~ # reboot
```
