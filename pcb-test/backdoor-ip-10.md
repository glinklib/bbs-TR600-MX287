后门IP登陆设备
===============
> 2017-4-6

# 1 用途
  首先后门IP不会影响设备的网络性能，固定IP地址 __10.0.10.10__，需要触发条件开启。

  生产环境若干台设备采用网络自动化测试情况必须IP不冲突，由此带来 __IP遗忘__问题。
  为了解决反复去询问同事该设备IP是多少，于是给设备添加 __双IP__ 功能，两IP均能连
  接设备，后门IP只有在满足情况下才被开启。

  后门IP只是用于查阅设备原始IP，不能直接用于生产，查阅原始IP后应该关闭后台IP，
  __防止后门IP冲突__ ，以不妨碍其他人用后台IP操作 __其他设备__。

# 2 操作
  
## 2.1 原始
  正常设备外网接口 __wan0__ 只有一个对外IP __192.168.0.201__

```
[root@TR600-Plus ~]# ifconfig 
lo        Link encap:Local Loopback  
          inet addr:127.0.0.1  Mask:255.0.0.0
          UP LOOPBACK RUNNING  MTU:16436  Metric:1
          RX packets:0 errors:0 dropped:0 overruns:0 frame:0
          TX packets:0 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:0 
          RX bytes:0 (0.0 B)  TX bytes:0 (0.0 B)

wan0      Link encap:Ethernet  HWaddr 00:04:66:6E:30:D6  Media:unknown(auto)
          inet addr:192.168.0.201  Bcast:192.168.0.255  Mask:255.255.255.0
          UP BROADCAST DEBUG NOTRAILERS PROMISC  MTU:1500  Metric:1
          RX packets:0 errors:0 dropped:0 overruns:0 frame:0
          TX packets:0 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000 
          RX bytes:0 (0.0 B)  TX bytes:0 (0.0 B)
```

## 2.2 修改PC地址
  将PC IP地址设置成 __10.0.10.0/24__ 子网除 10.0.10.10 以外任意IP

## 2.3 开启后台
  长按仪表面板 __Esc + Home__ 键 __8秒__

  此时设备以及开启了后台IP wan0:10 10.0.10.10__

```
[root@TR600-Plus ~]# ifconfig 
lo        Link encap:Local Loopback  
          inet addr:127.0.0.1  Mask:255.0.0.0
          UP LOOPBACK RUNNING  MTU:16436  Metric:1
          RX packets:0 errors:0 dropped:0 overruns:0 frame:0
          TX packets:0 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:0 
          RX bytes:0 (0.0 B)  TX bytes:0 (0.0 B)

wan0      Link encap:Ethernet  HWaddr 00:04:66:6E:30:D6  Media:unknown(auto)
          inet addr:192.168.0.201  Bcast:192.168.0.255  Mask:255.255.255.0
          UP BROADCAST DEBUG NOTRAILERS PROMISC  MTU:1500  Metric:1
          RX packets:0 errors:0 dropped:0 overruns:0 frame:0
          TX packets:0 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000 
          RX bytes:0 (0.0 B)  TX bytes:0 (0.0 B)

wan0:10   Link encap:Ethernet  HWaddr 00:04:66:6E:30:D6  Media:unknown(auto)
          inet addr:10.0.10.10  Bcast:10.0.10.255  Mask:255.255.255.0
          UP BROADCAST DEBUG NOTRAILERS PROMISC  MTU:1500  Metric:1
```

## 2.4 关闭后门
  使用后门IP进入系统后，知道对外IP wan0是192.168.0.201，后台IP的作用已经完毕，
  直接强制关闭设备，重启，__使用wan0的原始对外IP重新登陆__

  或者用命令直接用命令关闭后台IP

```
  ifconfig wan0:10 down
```
