测试：触屏响应
======================

工具 /app/rd-touch.elf

# 1 正常现象

  每次按下、提起触屏按压都以该有对应的down、up成对出现，中间的x、y是按压
  时的X、Y ADC采样值

  ![](../issues/images/rd-touch.png)

# 2 异常现象

  如果出现down、up不成对出现，则表示存在故障
  
  ![](../issues/images/rd-touch-bad.png)
