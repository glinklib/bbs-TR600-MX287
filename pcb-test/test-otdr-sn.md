# 测试：光耦各开关

被打开的开关为高电平，万用表量3.3V


- **打开所有开关**   

```
/app/fpga-2016-chip.elf --switch on
```

- **关闭所有开关**

```
/app/fpga-2016-chip.elf --switch off
```



- **只打开一个开关所有开关**

N 是被控制的开关，取值范围是 1-9  
**注意**  
如果 N = 0 则与 **关闭所有开关** 作用相同

```
/app/fpga-2016-chip.elf --switch N
```

