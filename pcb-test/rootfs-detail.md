文件系统部分文件作用简介
========================

# 1 /app
## 1.1 ./app/adc-t.elf
  ADC测试
## 1.2 ./app/bmp2png
  bmp转png工具，后期会整合到cscreen.elf
### 1.2.1 依赖
  - libpng.so

## 1.3 ./app/cscreen.elf
  屏幕抓图，保存成bmp，后期支持png
## 1.4 ./app/glink-desktop.elf
  Qt环境桌面，目前只用于系统升级弹出交互窗口
## 1.5 ./app/fpga-2016-chip.elf
  FPGA OTDR功能测试，具体使用参数不在这阐述，验证libfpga-2016.so可靠性
```shell
# -t 循环测试
# -d o 打印调试信息，OTDR数据缩略版
# -d r 打印接收数据
# -L 1310 物理通道：采用1310激光器
# --panel 1 逻辑通道：切换光开关，光从面板1号光口输出
/app/fpga-2016-chip.elf -t -d o -d r -L 1310 --panel 1
```
### 1.5.1 依赖
  - libfpga-2016.so

## 1.6 ./app/freebox
  网络配置提示符交互界面，用于后期运维
### 1.6.1 依赖
  - libminishell-ex.so
  - libreadline.so
  - libhistory.so

## 1.7 ./app/rd-keyboard.elf
  按键功能测试
## 1.8 ./app/shtcp.elf
  在shell环境完成TCP通信，与__./app/glink-desktop.elf__实现TCP短连接通信，用户在交互窗口按下什么按钮，像用户输出什么提示信息，都是靠它是命令，但效率很低，后期会改成有名管道通信

``` shell
# 向127.0.0.1:7310 建立TCP连接，显示消息窗口
/app/shtcp.elf -t 127.0.0.1 -p 7310 -s "wndshow  msg 1"
# 向127.0.0.1:7310 建立TCP连接，消息窗口显示 hello
/app/shtcp.elf -t 127.0.0.1 -p 7310 -s "append hello"
# 隐藏消息窗口
/app/shtcp.elf -t 127.0.0.1 -p 7310 -s "wndshow  msg 0"
```



## 1.9 ./app/TR600
  TR600-plus 正式应用程序
## 1.10 ./app/tr600-pp.elf
  TR600-plus 外围设备稳定性，验证libotdr-peripheral.so


<br><br><br><br>
--------------------------------
# 2 /bin
## 2.1 ./bin/mountdisk
  mdev 热插拔处理U盘、SD卡自动挂载、卸载、升级入口
### 2.1.1 依赖
  - mdev.conf

## 2.2 ./bin/renameif
  网口重命名
```shell
/bin/renameif eth0 wan0
```


## 2.3 ./sbin/modinfo
  输出软件基本信息，判断软件版本、配置，编译时间、sha1码  
![](images/modinfo.jpg)

<br><br><br><br>
--------------------------------
# 3 /etc
## 3.1 ./etc/clog.d/default
  日志文件压缩配置脚本，crond 定时任务之一。
  需要人为保证日志写入速度远远慢于日志压缩检测速度。
  在默认每开机2小时检测日志容量，超额则压缩或删除。
  
```shell
path=/var/log     # 日志路径
logfile=messages  # 日志名前缀
log_rotate=3      # 最多保存多少个压缩日志
compress_retate=5 # 每个压缩日志有多少个
```

  压缩后的结果
```shell
/var/log/messages         # 当前正在写入的日志
/var/log/messages.0       # 当前未被压缩的日志，日志建立顺序
/var/log/messages.1       # 5、4、3、2、1
/var/log/messages.2
/var/log/messages.3
/var/log/messages.4
/var/log/messages.0.tar  # 已结被压缩的日志
/var/log/messages.1.tar
/var/log/messages.2.tar
```
  具体每个未压缩文件占多少容量由syslogd来确定，默认配置256KB
```shell
syslogd -s 256 -b 4
```
## 3.2 ./etc/environment
  其内容可能供./etc/init.d/S12gpio 使用
```shell
# TR600-plus 定义3个端口，适配器，电池1、2检测
export DEVICE_PIN_POWER_ADAPTER="pin_power_adapter"
export DEVICE_PIN_BAT1_STATUS="pin_bat1_status"
export DEVICE_PIN_BAT1_STATUS="pin_bat2_status"
# HeBei-OTDR-V2.0 定义2个端口，1-2通道告警、3-4通道告警
export DEVICE_PIN_1_2_ALARM="pin_1_2_alarm"
export DEVICE_PIN_3_4_ALARM="pin_3_4_alarm"

```
  保证代码可移植性，C语言里通过如下形式获取环境变量
```c
#define DEF_PIN_BAT1 "pin_bat1_status"
static char *dev_pin_bat1 = DEF_PIN_BAT1;
env = getenv("DEVICE_PIN_BAT1_STATUS");
if (env) {
    dev_pin_bat1 = env;
}
```
## 3.3 ./etc/init.d/rcS
  启动脚本

<br><br><br><br>
## 3.4 ./etc/init.d/S*
  所有系统服务按照顺序启动，由__./etc/init.d/rcS__调用，每个服务统一基本接口
```shell
./etc/init.d/Sxx start      # 启动
./etc/init.d/Sxx restart    # 重启
./etc/init.d/Sxx stop       # 停止
```
### 3.4.1 ./etc/init.d/S12gpio
    将GPIO导出到用户空间，
```shell
# TR600 导出3个端口，默认输入口
echo 121 > /sys/class/gpio/export
echo 120 > /sys/class/gpio/export
echo 84 > /sys/class/gpio/export
```
  
  此部分依赖./etc/environment
```shell
# HeBei-OTDR-V2.0 将输入口改成输出
echo out > /sys/class/gpio/${DEVICE_PIN_1_2_ALARM}/direction
echo out > /sys/class/gpio/${DEVICE_PIN_3_4_ALARM}/direction
```

### 3.4.2 ./etc/init.d/S30fastapp
  再次自启动APP程序，如/app/TR600

  无关紧要应用放入 __./etc/init.d/S100normalapp__

### 3.4.3 ./etc/init.d/S40network
  启动并配置网络
### 3.4.4 依赖
  - ./etc/lan0-setting
  - ./etc/wan0-setting
  - ./bin/renameif

### 3.4.5 ./etc/init.d/S75telnetd
  启动telnet
### 3.4.6 依赖
  /bin/mount -a

### 3.4.7 ./etc/init.d/S80mount
  首次运行无脑挂载U盘和SD卡，因为没有热插拔事件/bin/mountdisk不得调用
### 3.4.8 ./etc/init.d/S99firstboot
  系统第一次开机运行脚本，配置环境，运行后自我删除
### 3.4.9 ./etc/init.d/S100normalapp
  一切准备就绪后运行应用
<br><br><br><br>




## 3.5 ./etc/[net]0-setting
  包括./etc/lan0-setting ./etc/usb0-setting ./etc/wan0-setting  
  用于配置外网、内网、9601USB网口

## 3.6 ./etc/mdev.conf
  热插拔配置
## 3.7 ./etc/pointercal(重要)
  LCD触屏校准，每次系统重新烧录后，必须用ts_calibrate校准
## 3.8 依赖
  - libts.so
  - ./usr/lib/ts/dejitter.so
  - ./usr/lib/ts/input.so
  - ./usr/lib/ts/linear.so
  - ./usr/lib/ts/pthres.so
  - ./usr/lib/ts/variance.so
  - ./etc/ts.conf
## 3.9 ./etc/ts.conf
  libts.so 配置


<br><br><br><br>
--------------------------------
# 4 /lib
./lib/libfpga-2016.so


# 5 ./lib/terminfo
  终端编码数据库，目前只支持vt100、vt102
  ./lib/terminfo/v/vt100 ./lib/terminfo/v/vt102
## 5.1 依赖
  - export TERMINFO=/lib/terminfo/




<br><br><br><br>
--------------------------------
# 6 ./opt
## 6.1 ./opt/qt-4.7.3/lib/fonts/wenquanyi_xxx_.qpf
  目前只存放wenquanyi字体库50、75号

<br><br><br><br>
--------------------------------
# 7 /usr
## 7.1 ./usr/bin/compresslog.sh
  压缩日志执行脚本
```shell
# 后面只有一个参数，参数是配置文件，配置文件描述压缩方式
/usr/bin/compresslog.sh /etc/clog.d/default
```


<br><br><br><br>
--------------------------------
## 7.2 /usr/lib
## 7.3 ./usr/lib/libramlog.so
  日志

## 7.4 ./usr/lib/libtr600plus-algo.so
  OTDR逻辑
### 7.4.1 依赖
  - libfpga-2016.so




<br><br><br><br>
--------------------------------
# 8 /var
## 8.1 ./var/spool/cron/crontabs/root
  自动化定时任务脚本
```shell
0 */2 * * * /usr/bin/compresslog.sh /etc/clog.d/default
```
