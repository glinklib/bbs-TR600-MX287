# 测试：LCD pwm是否正常

背光亮度范围 0 - 100（0关闭，100最亮）

- **查看背光亮度**
目前亮度
```
~ # cat /sys/class/backlight/mxs-bl/brightness 
80
```

- **设置背光亮度**
设置成20
```
~ # echo 50 > /sys/class/backlight/mxs-bl/brightness 
```
