测试：验证按键没有冲突
======================

工具 /app/rd-keyboard.elf

# 1 正常现象

按下一个按键有down、press两次，down、press应该 **整齐的间隔** 

```
~#/app/rd-keyboard.elf 
down  67 KEY_UP            <-- 按下
press 67 KEY_UP            <-- 自动重复
press 67 KEY_UP 
up    67 KEY_UP            <-- 抬起
down  6a KEY_RIGHT
press 6a KEY_RIGHT
up    67 KEY_RIGHT 
```

# 2 异常现象

如下分别按：上、下、左、右的结果，可以看到一次按下时 弹出两次down

右和下 两按键被短路，因为按下

 
```
~#/app/rd-keyboard.elf 
down  67 KEY_UP 
up    67 KEY_UP 
down  6a KEY_RIGHT  <--- 多余
down  6c KEY_DOWN   <--- 应该只有这一行
up    6a KEY_RIGHT  <--- 多余
up    6c KEY_DOWN   <--- 应该只有这一行
down  67 KEY_UP
up    67 KEY_UP
down  6a KEY_RIGHT  <--- 有问题
down  6c KEY_DOWN 
up    6a KEY_RIGHT 
up    6c KEY_DOWN
```
