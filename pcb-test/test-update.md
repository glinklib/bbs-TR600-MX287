# U盘/SD卡升级系统方法

## 1. 可升级内容 （生产人员必读）
----------------

  可选择如下任意1个或多个内容升级

  - **autorun.sh**  
  	非升级内容，用于升级意外的操作，比如导出日志、设备当前测试报告等
  - **logo.bmp**  
  	启动LOGO
  - **uImage**  
  	操作系统
  - **update.tar**  
  	文件系统升级补丁

  ![](./images/udisk.png)

### 注意
------------
> 原则上不允许在升级过程拔下存储器、掉电，有变砖风险，变砖后只能通过SD卡恢复，恢复方法另外述。

> uImage未升级完成100%变砖、update.tar未升级完成根据升级内容重要性有一定几率变砖，
> 升级run.sh logo.bmp任何时候被中断不影响系统

> 通常情况只提供 **常规版本** update.tar，系统缺陷才提供uImage，其余内容在出厂后不予提供



## 2. 升级操作（生产人员必读）

- 1 在存储器根目录下建立 **update** 文件夹
- 2 将需要升级的内容保存在该文件夹下
- 3 打开设备，设备启动完成后插入存储器弹出提示框，提示发现存储设备 /dev/sda1，
	10秒内未确认则忽略升级

![images](./images/update-1.png)

- 4 确定后分别执行 run.sh、升级内核、升级Logo、升级文件系统，该过程不允许断电。
	升级完成后窗口保持5秒提显示

![images](./images/update-2.png)


### 注意
------------
> 启动时如果已经插入U盘或SD卡，不会自动升级，必须有热插拔操作才能触发










<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
## 3 插入不同设备环境变量变化（研发人员选读）

---------------
### 3.1 多分区TF卡

```
PHYSDEVPATH=/class/mmc_host/mmc0/mmc0:0002
DEVTYPE=partition
SUBSYSTEM=block
DEVPATH=/block/mmcblk0/mmcblk0p1
PARTN=1
MINOR=1
PATH=/sbin:/bin:/usr/sbin:/usr/bin
ACTION=add
PWD=/dev
MAJOR=179
DEVNAME=mmcblk0p1
HOME=/
SHLVL=2
PHYSDEVDRIVER=mmcblk
MDEV=mmcblk0p1
PHYSDEVBUS=mmc
SEQNUM=1211
_=/usr/bin/env
end -------------
ACTION -
physdevyce -
mdev -
add action
PHYSDEVPATH=/class/mmc_host/mmc0/mmc0:0002
DEVTYPE=partition
SUBSYSTEM=block
DEVPATH=/block/mmcblk0/mmcblk0p3
PARTN=3
MINOR=3
PATH=/sbin:/bin:/usr/sbin:/usr/bin
ACTION=add
PWD=/dev
MAJOR=179
DEVNAME=mmcblk0p3
HOME=/
SHLVL=2
PHYSDEVDRIVER=mmcblk
MDEV=mmcblk0p3
PHYSDEVBUS=mmc
SEQNUM=1213
_=/usr/bin/env
end -------------
ACTION -
physdevyce -
PHYSDEVPATH=/class/mmc_host/mmc0/mmc0:0002
DEVTYPE=partition
SUBSYSTEM=block
DEVPATH=/block/mmcblk0/mmcblk0p2
PARTN=2
MINOR=2
PATH=/sbin:/bin:/usr/sbin:/usr/bin
ACTION=add
PWD=/dev
MAJOR=179
DEVNAME=mmcblk0p2
HOME=/
SHLVL=2
PHYSDEVDRIVER=mmcblk
MDEV=mmcblk0p2
PHYSDEVBUS=mmc
SEQNUM=1212
_=/usr/bin/env
end -------------
mdev -
add action
ACTION -
physdevyce -
mdev -
add action
mmcblk add /dev/mmcblk0p3 /mnt/sd
mmcblk add /dev/mmcblk0p1 /mnt/sd
mmcblk add /dev/mmcblk0p2 /mnt/sd
```

### 3/2 单分区TD卡
```
PHYSDEVPATH=/class/mmc_host/mmc0/mmc0:0002
DEVTYPE=disk
SUBSYSTEM=block
DEVPATH=/block/mmcblk6
NPARTS=0
MINOR=48
PATH=/sbin:/bin:/usr/sbin:/usr/bin
ACTION=add
PWD=/dev
MAJOR=179
DEVNAME=mmcblk6
HOME=/
SHLVL=2
PHYSDEVDRIVER=mmcblk
MDEV=mmcblk6
PHYSDEVBUS=mmc
SEQNUM=1198
_=/usr/bin/env
```

### 3.3 U盘
```
PHYSDEVPATH=/devices/platform/fsl-ehci/usb1/1-1/1-1:1.0/host1/target1:0:0/1:0:0:0
DEVTYPE=partition
SUBSYSTEM=block
DEVPATH=/block/sda/sda1
PARTN=1
MINOR=1
PATH=/sbin:/bin:/usr/sbin:/usr/bin
ACTION=add
PWD=/dev
MAJOR=8
DEVNAME=sda1
HOME=/
SHLVL=2
PHYSDEVDRIVER=sd
MDEV=sda1
PHYSDEVBUS=scsi
SEQNUM=1236
_=/usr/bin/env
```