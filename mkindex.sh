#!/bin/sh
cd ${1}
files=$(ls *.md   | grep -v "README.md")
for file in $files
do
	context=$(cat $file | head -n 1)
	# echo $context
	echo "- [${context}](./$file)"
done
