# 硬件缺陷：RTC保存提示Input/Output Error
> 2017-2.15

## 1. 现象
-------


保存/读取硬件RTC时钟时候错误

```
root@Hello:~# hwclock  -r
hwclock: RTC_RD_TIME: Input/output error
root@Hello:~# hwclock  -w
hwclock: RTC_SET_TIME: Input/output error
```



### 1.1 使用i2c-tools分析

总线上能正常穷举寄存器

```
[root@M28x nfs]# ./i2cdetect -y 1
     0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f
00:          -- -- -- -- -- -- -- -- -- -- -- -- -- 
10: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
20: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
30: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
40: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
50: -- UU -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
60: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
70: -- -- -- -- -- -- -- --   
```


读取寄存器时候错误，所有寄存器返回不能识别的字符

```
root@Hello:/nfs# ./i2cdump -f -y 1 0x51
No size specified (using byte-data access)
     0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f    0123456789abcdef
00: XX XX XX XX XX XX XX XX XX XX XX XX XX XX XX XX    XXXXXXXXXXXXXXXX
10: XX XX XX XX XX XX XX XX XX XX XX XX XX XX XX XX    XXXXXXXXXXXXXXXX
20: XX XX XX XX XX XX XX XX XX XX XX XX XX XX XX XX    XXXXXXXXXXXXXXXX
30: XX XX XX XX XX XX XX XX XX XX XX XX XX XX XX XX    XXXXXXXXXXXXXXXX
```

正常情况应该返回类似

```
[root@M28x nfs]# ./i2cdump -f -y 1 0x51
No size specified (using byte-data access)
     0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f    0123456789abcdef
00: 08 c0 d1 d1 d2 d4 d5 d0 16 e1 f3 c1 c1 d0 53 2f    ??????????????S/
10: 08 c0 d1 d1 d2 d4 d5 d0 16 e1 f3 c1 c1 d0 53 2f    ??????????????S/
20: 08 c0 d1 d1 d2 d4 d5 d0 16 e1 f3 c1 c1 d0 53 2f    ??????????????S/
```

XXX 的意思是没能从I2C总线上读取有效字符

<br><br><br><br>
## 2. 解决方案
-----------

倪艳海更换时钟芯片的电容即可正常运行，芯片手册介绍的外围电路所使用的电容公司没有，
焊接时随意用其他电容代替，导致以上问题。

现在更换成接近的电容基本可以正常运行。

**相关文章：**  
 [《PCF8563高温不工作》](./issues/issues011-rtc-50temp.md)