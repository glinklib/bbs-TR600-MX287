# 软件缺陷：MX287偶尔无法正常进入rootfs
在使用周立功开发板时就出现过类似的问题，大概出现过2-3次，再次硬复位可正常进入。

## 具体原因未知
- 有可能是调试期间办公桌凌乱导致


## 处理方法
在系统启动各阶段严格控制看门狗有效时间。如果系统没能进入rootfs，则在 **运行Kernel**
的20S后超时

![image](./images/MX287-boot-process.png)
