两按键同时repeat后抬起
=======================
> 2017-4-16

# 缺陷内核
  - 2017-4-16/uImage-v1.1-rc2

# 只收到一个

  使用rd-keyboard.elf测试键盘，同时按下任意两按键，使得其自动repeat，一定时间后
  __同时释放__ 按键，应用程序只能收到 __一个按键的up事件__

```
down  KEY_A  <-- 先按下A
press KEY_A  <-- 自动repeat A
press KEY_A
press KEY_A
press KEY_A
press KEY_A

down  KEY_B  <-- 不放开A，同时按下B
press KEY_B  <-- 自动repeat B
press KEY_A  <-- A、B的事件交替
press KEY_B
press KEY_A
press KEY_B
press KEY_A
press KEY_B
press KEY_A

up    KEY_B <-- 同时释放A、B，只收到B的up事件
```

# 修复后

```
down  KEY_A  <-- 先按下A
press KEY_A  <-- 自动repeat A
press KEY_A
press KEY_A
press KEY_A
press KEY_A

down  KEY_B  <-- 不放开A，同时按下B
press KEY_B  <-- 自动repeat B，不存在交替A、B
press KEY_B
press KEY_B
press KEY_B
press KEY_B
press KEY_B

up    KEY_B  <-- 同时释放A、B，收到两事件
up    KEY_A
```
