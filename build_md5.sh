#!/bin/sh
function create_file()
{
path=$1

files=$(ls ${path} | grep -v README.md | grep -v patch)


echo "# ${path}"
echo "文件 | MD5"
echo "------ | ------"



for file in ${files}
do	
	F_FILE=${file}
	if [ "${file}" != 'brief.md' ]
	then
		F_MD5=$(md5sum ${path}/${file} | awk '{print $1}')
		
		echo "${F_FILE} | ${F_MD5}"
	fi
	
done


}

create_file $1  >  $1/README.md
echo -e '\r\n' >> $1/README.md
cat  $1/brief.md >> $1/README.md