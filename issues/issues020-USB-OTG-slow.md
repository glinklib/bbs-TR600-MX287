USB OTG插入电脑导致系统慢 
=========================
> 2017-12-19

# 现象
  - 插入OTG线
  - 再在USB Device口插入U盘、USB网卡、USB鼠标等

telnet登陆感觉输入反馈很慢

```
[root@TR600-Plus ~]# ps
PID   USER     TIME   COMMAND
    1 root       0:03 init
    2 root       0:00 [kthreadd]
    3 root       0:00 [ksoftirqd/0]
    4 root       0:00 [events/0]
    5 root       0:00 [khelper]
    8 root       0:00 [async/mgr]
    9 root       0:00 [pm]
   64 root       0:34 [usb_wakeup thre]  <------------
  204 root       0:00 [sync_supers]
  206 root       0:00 [bdi-default]
```

  - 拔掉USB Device设备不能恢复
  - 拔掉OTG线恢复

# 解释
  以上的使用方法是复用了OTG的主从设备功能。
  
  实际上开发板上的OTG控制器是同时连接了主设备和从设备的USB端子（USB1和OTG），
  当OTG插入电脑后，就不该同事在USB1上插入U盘，否则会导致系统无法判定当前该
  控制器处于哪种工作模式，现象就是__特别卡__。
  只要同一时刻只选择一种模式就不会出现上面的现象。
