mdev -s引起SD卡挂载失败
==========================

> 2017-5-6


# 1 现象
  系统启动后不能自动挂载u盘、SD卡，只有插拔后才能挂载成功

# 2 原因
 由[《加快系统启动实际措施》](../feature/optimize-run-stream.md)导致，
 实际上SD卡已经挂载成功，只是所有启动脚本运行结束后执行 __"mdev -s"__ 导致所有
 挂载设备 __复位__ 。

# 3 措施
## 3.1 方法1
  在S10pre-mdev，下添加如下内容，能在S80mount里自动挂载成功

```
mknod /dev/mmcblk0 b 179 0
mknod /dev/mmcblk0p1 b 179 1
mknod /dev/mmcblk0p2 b 179 2
mknod /dev/mmcblk0p3 b 179 3

mknod /dev/sda  b 8 0
mknod /dev/sda1 b 8 1
```

## 3.2 方法2
  创建另一组启动脚本 __Smdev-nnXXXX__，如：Smdev-80mount，次组脚本在mdev -s后
  才运行，而其他对用户体验要求高的启动脚本保持 __SnnXXX__ 命名规则

  执行顺序

```

/etc/init.d/S10pre-mdev
/etc/init.d/S12gpio
/etc/init.d/S20network
/etc/init.d/S30fastapp            <---- 系统相关的应用程序在此运行
/etc/init.d/S75telnetd
/etc/init.d/S99normalapp

mdev -s                           <---- 比较耗时的创建设备节点
/etc/init.d/Smdev-80mount         <---- 挂载U盘、SD卡
```


