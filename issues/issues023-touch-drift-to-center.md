鼠标坐标漂移到中央
============
> 吴梦龙  
> 2017-12-21

# 1 现象

  - 快速点击屏幕__任意位置__，触屏坐标很容易漂移到屏幕中央
  - 在TR600 OTDR测试界面，快速点击__曲线缩放按钮__，屏幕中央的__AB标杆切换按钮__
  被误触
  
![](images/issues023/tr600-1.png)

  - 同理在设置菜单里点击标签选项卡通高度的
  
![](images/issues023/tr600-2.png)

  - 移植UCGUI到MX287，触屏按压坐标位置没有问题，当手指离开触摸屏后，鼠标移到到
  屏幕__X轴中央__
  - 变更触屏驱动，原始触屏坐标检测顺序是__先检测Y轴后检测X轴__，将其
  顺序对调。重新测试UCGUI，当手指离开触屏后，鼠标移到到屏幕__Y轴中央__


# 2 信号解释
# 2.1 MX287触屏控制器
  - 硬件接线与示波器探针位置  
  
  __使用方法__：
  
  - 空闲时候，X-接+（3.3V），Y-接0，X+接+，Y+接0
  - 按下时候：处理器探测到X-下降到触发电平（触发电平大约2V，触摸电平也在0.5V以上）,触摸有效范围大约是0.5V~2V
  - 探测X轴：X-断路，X+作为ADC输入，Y+接+，Y-接-
  - 探测Y轴：Y-断路，Y+作为ADC输入，X+接+，X-接-

![信号](images/issues023/pcb.png)
  
  - __没有按下__时信号  
![信号](images/issues023/release.png)

  - 正常（mx287）触屏__抬起__信号  
![](images/issues023/press-1.png)
  
  - 快速点击屏幕统__正常信号__  

![22](images/issues023/fast-click-1.png)

  - 快速点击屏幕统__异常信号__  

  __解释Y轴上的斜边__：驱动中处理器在得知触屏按下后，关闭touch detect（触屏探测功能），使得电平被锁住，但依旧有缓慢放电，__设计目的__ 
  是为了ADC能有足够的时间采集信号  
![](images/issues023/fast-click-2.png)

  - 上文中UCGUI里触摸抬起后坐标回到中央的时序  
![信号](images/issues023/long-press-up.png)
  

  - 使用rd-touch.elf观察X、Y轴ADC值分布

  __标准差mx-ts:x 轴309.89，y 轴 308.58__    
![](images/issues023/plot-mx-ts.png)

# 2.1 补充ADS7843触屏控制器信号
  ADS7843触屏信号与mx287有不同，是一个只有几us的脉冲，没有明显的阶梯形状
  
  - 手按压在触屏上，每100ms读取一次触屏ADC  
![](images/issues023/ad-1.png)

  - 放大每个脉冲，实际上是4个脉冲值，每个脉冲得到一组X、Y ADC值，
  ADC转换时间约20us  
![](images/issues023/ad-2.png)


  - 读取快速点击屏幕，点击/抬起时间大约在100ms左右，
  无论是在点击、还是释放的过程中，都可以进行AD采样。
  这是与MX287触屏控制器的本质不同
![](images/issues023/ad-3.png)
  ADS7843驱动读取ADC过程

```
void TP_StartADC()
{
    struct point ad, pt;
    // 4次采样
    TP_GetAdXY(&x[0], &y[0]);
    TP_GetAdXY(&x[1], &y[1]);
    TP_GetAdXY(&x[2], &y[2]);
    TP_GetAdXY(&x[3], &y[3]);

    // 4组数据滤波后满足稳定度条件
    if (fliter() ) {
        sprintf(dbgout,"%d %d\n\n%d %d\n%d %d \n%d %d\n%d %d\n",xdata,ydata,x[0], y[0],
            x[1], y[1],
            x[2], y[2],
            x[3], y[3]);
        // 保持按下状态，等待100ms再次读取
        if (TP_IsPress()) {
            g_touch_ms = 100;
        }
        
        // 报告坐标
        pt.x = tpadj.A1 * xdata + tpadj.B1 * ydata + tpadj.C1;
        pt.y = tpadj.A2 * xdata + tpadj.B2 * ydata + tpadj.C2;  
        GUI_TOUCH_StoreUnstable(pt.x, pt.y);
    }
    else {
        // 滤波发下数据偏差大，10ms后再次读取
        if (TP_IsPress()) {
            g_touch_ms = 10;
        }
    }   
}

// 触屏按下、抬起中断
void EXTI0_IRQHandler(void) 
{
    if(EXTI_GetITStatus(EXTI_Line0) != RESET)
    {
        // 如果是按下状态，则等待40ms后取触屏数据
        if (TP_IsPress()) {
            g_touch_ms = 40;
        }
        // 否则上报触屏抬起
        else {
            GUI_TOUCH_StoreUnstable(-1, -1);
        }
        EXTI_ClearITPendingBit(EXTI_Line0);
    }
}
```
# 3 重写驱动
  重写mxs-ts.c改名为mxs-ts-glink.c， __驱动特点：__
  
  - 禁用touch detect 中断（原中断没有这个操作）
  - 这个过程每几ms定时器判断touch是否按下
  - 整个过程对按键是否按下、抬起判断准确
  - 坐标测试顺序先x轴，后y轴，在最后1个点y轴ADC值普遍在10以内，该值丢弃
  - ADC信号伏值很低，一次探测花费36ms

![](images/issues023/glink-small.png)

   - 使用rd-touch.elf观察分布
   
   __标准差mx-ts-glink:x 轴93.55，y 轴  19.917__  
![](images/issues023/plot-mx-ts-glink.png)

## 3.1 测试新驱动
  - 手指按在一个新位置上，Y轴准确，X轴需要2秒后才稳定，X轴坐标位于手指处左方1.5cm处，然后__缓慢靠近__触屏位置
![](images/issues023/mxs-ts-glink-test.png)

  __解释__  
  没有断开touch detect，ADC管脚充电速度太慢
  
  - 手指保持在同一位置上，触摸屏__抖动__明显
  
  __解释__  
  因为动态范围太小，原始驱动有1500个ADC，新驱动只有数百

# 4 重写驱动不做平均过滤
  
  - mxs-ts 原驱动
    - __mx-ts.csv x轴  309.895 Y轴 308.58__
  - ts-glink 同时获取触屏状态和触屏坐标
    - __ts-glink.csv x轴 93.556 Y轴 19.917__
  - deta-xy-xx 不做任何平均，仅对x、y轴分别与之前的坐标做判断
    - __deta-xy-100.csv x 轴 39.336 Y轴 13.059__
    - __deta-xy-100-2.csv x轴 35.381 Y轴 24.007__

![](images/issues023/plot-mx-ts-deta.png)  
  
  新处理逻辑基本可以解决两个问题
  
  1. 快速点击屏幕统__异常信号__
  2. 快速点击屏幕__任意位置__，触屏坐标很容易漂移到屏幕中央
  
## 4.1 问题  
  使用rd-touch.elf观察触屏点时发现x轴的坐标个数略多于Y轴，不知是哪里的问题。
  在QT上不会引起什么问题，但移植uCGUI或许会导致坐标逻辑混乱，uCGUI里不能想当然
  的认为x、y的数据时同时存在的。
  
# 结论
  - mxs-ts.c 关闭touch detect探测，导致快速点击出现问题，触屏抬起出现问题
  - mxs-ts-glink.c虽然ADC只稳定，但动态范围太小，屏幕上长按某点抖动

# 5 建议
  不用mx287的触摸接口，换用ADS7843 SPI接口，或者用电容屏的I2C接口

