支持deb安装包
=============

# 1 什么是deb包

  DEB是Debian软件包格式的文件扩展名。__用于软件安装__。deb包在Linux操作系统中类似于windows中的软件包（exe)，几乎不需要什么复杂的编译即可通过鼠标点击安装使用
  
  Debian包是Unixar的标准归档，将包文件信息以及包内容，经过gzip和tar打包而成。
  
  处理这些包的经典程序是dpkg，经常是通过Debian的apt-get来运作。
  
  通过Alien工具，可以将deb包转换成rpm、tar.gz格式。
  
  Bbusybox支持 __精简版__ 的dpkg安装和卸载deb包。

  如何制作deb包参考
  
  - [《用dpkg命令制作deb包方法总结》][1]
  - [Glink MX287支持的部分安装包][2]
  
  deb包的支持从TF500开始。TR600、TRK200暂时不考虑。


# 2 为什么采用deb
  使得软件的安装和卸载更简单，无需开发人员亲临指导，只需要在U盘里放入 xxx.deb 选择安装即可。同样对于安装后安装的文件位置被记录在设备中，可在不需要的时候卸载软件。
  
  并且在安装和卸载时能够检查软件依赖，防止某软件卸载后影响其他软件运行

## 2.1 例子
  安装php7，提示依赖boa包

```
[root@TRK200 nfs]# dpkg -i php-7.1.21-boa-mini.deb 
dpkg: package php7 depends on boa, which is not installed or flagged to be installed
```
  
  依次安装boa和php7

```
[root@TRK200 nfs]# dpkg  -i boa-0.94.13.deb 
Unpacking boa (from boa-0.94.13.deb)...
Setting up boa (0.94.13)...
[root@TRK200 nfs]# dpkg -i php-7.1.21-boa-mini.deb 
Unpacking php7 (from php-7.1.21-boa-mini.deb)...
Setting up php7 (7.1.21)...
```
  查看已经安装的软件
  
```
[root@TRK200 /]# dpkg  -l
    Name           Version
+++-==============-==============
ii  boa            0.94.13
ii  php7           7.1.21
```
  查看deb包软件安装的文件

```
[root@TRK200 nfs]# cat /var/lib/dpkg/info/boa.list 
/.
/srv
/srv/web
/srv/web/index.html
/srv/web/cgi-bin
/srv/web/cgi-bin/test.cgi
/etc
/etc/boa
/etc/boa/boa.conf
/etc/boa/mime.types
/etc/mime.types
/etc/init.d
/etc/init.d/S90boa
/sbin
/sbin/boa
/cgi-bin
```

  若要卸载某软件，dpkg会检查该软件是否有依赖，如：卸载boa，不能直接卸载，因为php7依赖他

```
[root@TRK200 nfs]# dpkg  -P boa
dpkg: package php7 depends on boa, which is not installed or flagged to be installed
```
  能够直接卸载php7

```
[root@TRK200 nfs]# dpkg  -P php7
Purging php7 (7.1.21)...
```
  
# 3 与以前的tar包安装有什么不同
  - __最大区别__：deb在安装后能记录新增文件信息，待磁盘容量过大时可卸载，tar无法提供该功能
  - tar包本质是个压缩包，只能在原文件系统基础上新增文件、不能删除
  - 每个deb文件一般都比较小，而tar可能很大，文件系统如果没用足够的空余空间将不能完全解压，有可能导致系统依赖文件损坏（其实deb过大也有可能）
  - 便于软件功能分模块发布，如wifi、蓝牙芯片版本太多，不能完全编译到固件中，所以可根据市场需求编译若干模块的deb包，告知用户产品支持的模块类型，用户购买模块后自行下载相应的deb即可驱动模块

# 4 dpkg UI界面
  qt-dpkg是一个以dpkg为基础的QT UI界面，使用它可以直接在LCD上操作。
  
  
  主界面，可对软件进行安装和卸载
  
  ![](images/qt-dpkg.png)
  
  选择install可查看某目录下的deb包
  
  ![](images/install.png)
  
  选择browse进入其他存在deb的目录
  
  ![](images/browse.png)
  
  选择Purge可看到当前系统可卸载的软件
  
  ![](images/purge.png)

## 4.1 自动安装
  
  如下两个命令可以完成自动安装，qt-dpkg提供QT界面，而dpkg-crt无界面提示
  
  它们搜索指定目录下的配置文件 __debs.conf__，并按配置文件的顺序安装目录下的deb包


```
  qt-dpkg.elf -a /mnt/data/debs
  或者
  dpkg-crt.elf -a /mnt/data/debs
```

  指定路径下的内容

```
[root@TRK200 nfs]# ls /mnt/data/debs/   
boa-0.94.13.deb                     <---- 2. 被自动安装的文件
debs.conf                           <---- 1. 读取自动安装配置
otdr-base-app-2018-11.deb
otdr-base-lib-2018-11.deb
php-5.6.37-boa-mini.deb
php-7.1.21-boa-mini.deb             <---- 2. 被自动安装的文件
tms400-otdr-2018-11.deb
```

  debs.conf的内容
  
```
[root@TRK200 nfs]# cat /mnt/data/debs/debs.conf 
boa-0.94.13.deb                      <---- 文件排序决定安装顺序
php-7.1.21-boa-mini.deb
```


  qt-dpkg.elf 软件自行时自动安装界面

  ![](images/auto-install.png)

  dpkg-crt.elf 首次安装，输出信息
  
  ![](images/dpkg-crt-1.png)
  
  dpkg-crt.elf 再次安装，输出信息  
  
  ![](images/dpkg-crt-2.png)

  [1]: http://www.xuebuyuan.com/937430.html
  [2]: https://gitee.com/glinklib/arm-debs