加快系统启动实际措施
====================

  本文给出[《Linux启动耗时》](./run-stream.md) 涉及的问题给予解决方案

# 1 优化mdev
  mdev -s 加载系统所有设备节点，耗时大约5秒，如果该任务放在后面运行，则会导致
  依赖某dev的设备无法运行

  解决措施是
  - 手动创建必要的设备节点
  - 启动APP
  - mdev -s 创建其他设备节点

## 1.1 必须的设备节点
```
crw-rw----    1 root     root        4,   0 Sep 23 01:08 /dev/tty0
crw-rw----    1 root     root        4,   1 Sep 23 01:08 /dev/tty1
crw-rw----    1 root     root        4,   2 Sep 23 01:08 /dev/tty2
crw-rw----    1 root     root        4,   3 Sep 23 01:08 /dev/tty3
crw-rw----    1 root     root        4,   4 Sep 23 01:08 /dev/tty4

crw-rw----    1 root     root      153,   0 Sep 23 01:08 /dev/spidev3.0
crw-rw----    1 root     root       29,   0 Sep 23 01:08 /dev/fb0


crw-rw----    1 root     root        5,   0 Sep 23 01:17 tty
crw-rw----    1 root     root        5,   1 Sep 23 01:17 console
crw-rw----    1 root     root        5,   2 Sep 23 01:08 ptmx


drwxr-xr-x    2 root     root           100 Sep 23 01:17 input
crw-rw----    1 root     root       13,  64 Sep 23 01:17 /dev/input/event0
crw-rw----    1 root     root       13,  65 Sep 23 01:17 /dev/input/event1
crw-rw----    1 root     root       13,  66 Sep 23 01:17 /dev/input/event2
```


## 1.2 手动创建
```
mknod /dev/tty0      c 4 0
mknod /dev/tty1      c 4 1
mknod /dev/tty2      c 4 2
mknod /dev/tty3      c 4 3
mknod /dev/tty4      c 4 4

mknod /dev/spidev3.0 c 153 0
mknod /dev/fb0       c 29 0

mknod /dev/console c 5 0
mknod /dev/tty     c 5 1
mknod /dev/ptmx    c 5 2

mkdir /dev/input # 包括必要的文件夹
mknod /dev/input/event0 c 13 64
mknod /dev/input/event1 c 13 65
mknod /dev/input/event2 c 13 66
```

## 1.3 优化效果

### 1.3.1 优化前

  - 6.9秒显示logo
  - 19.9秒看到鼠标
  - 21秒APP启动完成

### 1.3.2 优化后

  - 6.9秒显示logo
  - 16.2秒看到鼠标
  - 17.4秒APP启动完成
