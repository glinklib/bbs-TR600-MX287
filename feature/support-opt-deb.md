系统从第二分区安装deb包
=================

# 1. 参考

 - [支持deb安装包](./qt-dpkg.md)

# 2. 为什么使用
  为了文件系统保持最精简，避免项目/app/目录下的应用程序变动，而需要重新制作根文件系统镜像的繁琐操作(rootfs.ubifs)。
  而/app下的应用程序（以及它所依赖的/usr/lib/)作为随时可卸载、可更新部件，分散到各个开发人员自己维护，并制作deb包，不再绑定到rootfs.ubifs中。
  
  根文件系统视为纯净系统，只保证如下功能：
  
  - 挂载第二分区磁盘
  - 创建/dev/下文件节点
  - 远程可telnet登陆
  - ftp可登陆放入升级文件，xxx-update.tar 或 xxx.deb
  - 检查/mnt/data/update是否有升级需求
  - 检查/mnt/data/deb是否有升级需求
  - U盘插入触发升级

# 3. 系统从第二分区安装deb包流程
## 3.1 __载入安装各deb包__

  系统启动最后一个加载任务是检查/mnt/data/deb下是否有安装包，
  任务位于 /etc/init.d/S99normal，使用还是dpkg-crt或qt-dpkg依赖于设备是否有屏幕

```
[root@M-OTDR[M-OTDR-V3_1] /]# cat /etc/init.d/S99normalapp 
#!/bin/bash
/app/dpkg-crt.elf -a /mnt/data/debs &
# /app/qt-dpkg-crt.elf -a /mnt/data/debs &
```

## 3.2. 等待安装完成
  
  - 对于有界面的设备，可看到安装过程进度
  - 对于无界面的设备，只能通过运行灯、错误LED的闪烁亮灭提示
  
  安装完成后 __手动__ 重启

## 3.3. 自启动具体应用
  在 __步骤3.1__ 执行时，deb包应该会在下生成自启动脚本 __/etc/init.d/SxxTRxx__ ，从而启动项目相关的应用程序
  
  如TMS400-OTDR项目里的/etc/init.d/S30tms400-otdr 内容

```
[root@M-OTDR[M-OTDR-V3_1] /]# cat /etc/init.d/S30tms400-otdr
#!/bin/sh
/app/run_app.sh &
```

  
# 4. 变动
  uboot变动
  
  提供第二分区 opt.ubifs 烧录

```

SHA1 c5f651f654b427973bc0129873c752af6e6dbb24
##### Glink [uboot] Nov 14 2018 16:37:05#####
SN: ?????????????
[a]  一键下载下面4项
[1]  专用版本 下载uboot    M-OTDR[M-OTDR-V3_1]-uboot.bin
[2]  专用版本 下载kernel   M-OTDR[M-OTDR-V3_1]-uImage
[31] 专用版本 下载rootfs   M-OTDR[M-OTDR-V3_1]-rootfs.ubifs
[32] 专用版本 下载opt      M-OTDR[M-OTDR-V3_1]-opt.ubifs        <----- 新增
[4]  专用版本 下载logo     M-OTDR[M-OTDR-V3_1]-logo.bmp M-OTDR[M-OTDR-V3_1]-ubootlogo.bmp
-----------------------------------
[5]  通用版本 下载uboot
[6]  通用版本 下载kernel
[71] 通用版本 下载rootfs
[72] 通用版本 下载opt                                           <----- 新增
[8]  通用版本 下载logo
[9]  通用版本 下载ubootlogo
```

  磁盘挂载变动

  将第二分区挂载到 /mnt/data 目录，容量45M

```
[root@M-OTDR[M-OTDR-V3_1] nfs]# ./busybox df -h
Filesystem                Size      Used Available Use% Mounted on
ubi0:rootfs              54.4M      8.8M     45.6M  16% /
tmpfs                    60.8M         0     60.8M   0% /tmp
tmpfs                    60.8M      4.0K     60.8M   0% /dev
ubi1:opt                 40.8M      4.4M     36.3M  11% /mnt/data   <----- 新增
```


# 例子：生成deb包集合
  以tms400为例，依赖
  
  - wjc.elf 文金朝维护 __tms400-otdr-2018-11.deb__
  - libfpga-tr700.so 吴梦龙维护 __otdr-base-lib-2018-11.deb__

  如何制作deb包参考
  
  - [《用dpkg命令制作deb包方法总结》][1]

  tms400-otdr-2018-11目录下内容

```
├── app
│   ├── run_app.sh
│   ├── sh
│   │   ├── cfg_dev.sh
│   │   ├── cron_wjc.sh
│   │   ├── monitor_disk.sh
│   │   ├── monitor_log.sh
│   │   ├── ping.sh
│   │   └── S12gpio-400
│   ├── wjc-d.elf
│   └── wjc.elf
├── DEBIAN
│   └── control
└── etc
    └── init.d
        └── S30tms400-otdr
```
  tms400-otdr-2018-11.deb描述信息
  tms400-otdr-2018-11/DEBIAN/control 
  
```
$ cat DEBIAN/control 
Package: tms400-otdr-wjc
Version: 1.0
Architecture: arm
Maintainer: No have
Depends:
Description: xxx
```

  otdr-base-lib-2018-11目录下内容
  
```
├── DEBIAN
│   └── control
└── usr
    └── lib
        ├── libfpga-tr700.so
        └── libotdr-peripheral.so
```

  otdr-base-lib-2018-11.deb描述信息
  otdr-base-lib-2018-11/DEBIAN/control 

```
Package: otdr-base-lib
Version: 2.3.0
Architecture: arm
Maintainer: No have
Depends:
Description: xxx
```

  自动安装配置信息 debs.conf

```
otdr-base-lib-2018-11.deb
tms400-otdr-2018-11.deb
```

[1]: http://www.xuebuyuan.com/937430.html