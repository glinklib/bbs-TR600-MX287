Linux启动耗时
=============

  下面是启动过程时序图，单位是秒

```
0-------------5-----------10---------10.69--------------14.9-----15.12-------15.9------16---20
^   ^         ^           ^          ^                  ^        ^           ^         ^     ^
|   |         |           |          |                  |        |           |         |     | APP显示
|   |         |           |          |                  |        |           |         | 其他可以放在后面运行的进程
|   |         |           |          |                  |        |           | 执行APP
|   |         |           |          |                  |        | 初始化GPIO、网卡
|   |         |           |          |                  | 开启日志系统
|   |         |           |          | 检查SD、U盘（最耗时）
|   |         |           | 内核运行完毕，开始执行rcS
|   |         | 内核运行
|   | 载入内核
| 按下开机键，启动uboot
```

  对于启动过程优化参考

  - [《加快系统启动实际措施》](./optimize-run-stream.md)

# 1 系统启动过程打印输出


## 1.1 U-Boot

```
U-Boot 2009.08-dirty (Feb 15 2017 - 08:59:45)

Freescale i.MX28 family
CPU:   454 MHz
BUS:   151 MHz
EMI:   205 MHz
GPMI:   24 MHz
init tr600 board
NAND:  nand_init 90
maf_id 1
mfr_id: 1 
nand_device_info_fn_spansion called.
nand slc
manufacturer_code      : 1
Manufacturer      : Spansion (0x01)
Device Code       : 0xf1
Cell Technology   : SLC
Chip Size         : 128 MiB
Pages per Block   : 64
Page Geometry     : 2048+64
ECC Strength      : 4 bits
ECC Size          : 512 B
Data Setup Time   : 20 ns
Data Hold Time    : 10 ns
Address Setup Time: 20 ns
GPMI Sample Delay : 6 ns
tREA              : Unknown
tRLOH             : Unknown
tRHOH             : Unknown
Description       : S34ML01G2
128 MiB
```

## 1.2 U-boot载入内核（耗时5秒）

```
NAND read: device 0 offset 0x200000, size 0x300000
 3145728 bytes read: OK
## Booting kernel from Legacy Image at 40400000 ...
   Image Name:   Linux-2.6.35.3-571-gcca29a0+
   Image Type:   ARM Linux Kernel Image (uncompressed)
   Data Size:    2716320 Bytes =  2.6 MB
   Load Address: 40008000
   Entry Point:  40008000
```

## 1.3 Linux
  前面的数值是时间搓，因为linux启动时在第5秒，所以计算时候响应加5秒

```
Starting kernel ...

Uncompressing Linux... done, booting the kernel.
[    0.000000] Linux version 2.6.35.3-571-gcca29a0+ (root@UbuntuServer) (gcc version 4.4.4 (4.4.4_09.06.2010) ) #158 PREEMPT Sat Apr 8 14:03:47 CST 2017 00c2417443125ac325648fd
[    0.000000] CPU: ARM926EJ-S [41069265] revision 5 (ARMv5TEJ), cr=00053177
[    0.000000] CPU: VIVT data cache, VIVT instruction cache
*********************************************************
           省略若干
*********************************************************
[    5.040000] UBIFS: reserved for root:  0 bytes (0 KiB)
[    5.050000] VFS: Mounted root (ubifs filesystem) on device 0:15.
[    5.060000] Freeing init memory: 948K
```

## 1.4 执行rcS（耗时6-8秒）
  线面的 __<----__ 是rcS执行某命令的时间搓


```
cat: can't open '/proc/uptime': No such file or directory
rcS
cat: can't open '/proc/uptime': No such file or directory
mount -a
5.64 1.74   <----
mkdir
5.69 1.75   <----
/sbin/mdev -s
umount: can't umount /mnt/sd: Invalid argument
9.81 1.97   <----
syslogd
9.86 1.97   <----
crond
9.90 1.97   <----
env
9.92 1.97   <----
profile
10.04 2.00   <----
/etc/init.d/S12gpio
10.12 2.00   <----
/etc/init.d/S20network
[   10.330000] wan0: Freescale FEC PHY driver [Generic PHY] (mii_bus:phy_addr=0:05, irq=-1)
[   10.410000] device wan0 entered promiscuous mode
[   10.460000] lan0: Freescale FEC PHY driver [Generic PHY] (mii_bus:phy_addr=0:01, irq=-1)
[   10.520000] device lan0 entered promiscuous mode
[   10.640000] wan0: Freescale FEC PHY driver [Generic PHY] (mii_bus:phy_addr=0:05, irq=-1)
[   10.840000] lan0: Freescale FEC PHY driver [Generic PHY] (mii_bus:phy_addr=0:01, irq=-1)
10.93 2.13   <----
/etc/init.d/S30fastapp                  <----------- 执行APP
10.97 2.13   <----
/etc/init.d/S75telnetd
11.20 2.15   <----
/etc/init.d/S80mount
/etc/init.d/S80mount: line 3: [: too many arguments
```

