MX287-Linux 版本修改概要
========================
# v1.1-rc5
  - Fix(lcd):LCD显示LOGO前花屏1S

# V1.1-rc4
  - 备份.config 到 config-board
  - spi_mxs 添加delay_usecs，从而提供STM32 SPI处理响应时间
  - 支持usb dm9601
  - LCD背光亮度持续时间控制 sys/class/backlight/mxs-bl/timeout 
  - 支持从uboot传递序列号到环境变量SN，如果没传递则没有该环境变量
  - 看门狗保护启动过程
  - 支持 7寸 1024x640 LCD，此LCD由外部接RGB转LVDS接口

```
setenv bootargs 'gpmi=g console=ttyAM0,115200n8 ubi.mtd=5 root=ubi0:rootfs rootfstype=ubifs fec_mac= ethact mem=128M SN=xxx'
```

# V1.1-rc3
  - gpio-keys 开启repeat属性
  - Glink特定的SPI协议，支持一次性最大传输 32000 * 4 + 4000 * 2 Byte
  - 使用sha1
  - 假的启动进度条
  - LCD背光反向
  - 启动LOGO
  - 驱动mxs-glink-lradc支持7个ADC通道
  - 多进程可同时打开看门狗
  - 蜂鸣器和LED指示启动过程
  - 电源按键长按7秒强制断电
  - 取消原始固件里对 GPIO 命名 pin_bank 标签的检查
  - 支持 5.1寸 奇美 LCD 640x480

