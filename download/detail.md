下载常见问题解释与处理
================

# 1 MsgTool不识别
  正常上电蜂鸣器会 __“滴”__ 一声，USB方式启动没有声音  

## 1.1 处理方式
  检查WDG、USB是否短接，WDG需要长期断就，USB只是上电瞬间短接即可。
  ![](./images/connect-2.jpg)

# 2 USB连接状态
  未连接  
  ![](./images/detail-1.png)  

## 2.1 处理方式
  - 重新上电，
  - options->configuration  检查USB Ports是否自动检测到 __HID-compliant device__  
  - 点击扫描设备  

  ![](./images/detail-3.png)  
  已连接  
  ![](./images/detail-2.png)  
  


# 3 uboot
## 3.1 主菜单
```
SHA1 7b5f7ee3e0b7706e90ec7e520c5933602d602009
##### Glink [uboot] Apr 18 2017 11:45:52#####
[a] 一键下载下面4项
[1] 专用版本 下载uboot    TR600-Plus-uboot.bin
[2] 专用版本 下载kernel   TR600-Plus-uImage
[3] 专用版本 下载rootfs   TR600-Plus-rootfs.ubifs
[4] 专用版本 下载logo     TR600-Plus-logo.bmp
-----------------------------------
[5] 通用版本 下载uboot
[6] 通用版本 下载kernel
[7] 通用版本 下载rootfs
[8] 通用版本 下载logo
-----------------------------------
[bn] 从Nand启动
[f] 格式化分区
[d] 调试uboot
[k] 调试内核
[ ] TODO 校验内核
[s] 配置 
[r] 重启 
[q] 退出 
TR600-Plus > 
```
  - 版本唯一序列号 __7b5f7ee3e0b7706e90ec7e520c5933602d602009__
  - 编译时间 __Apr 18 2017 11:45:52__  
    通过这个时间可以提交给研发，确定某些由版本导致的问题
  - 提示符 __TR600-Plus > __
  - 专用版本 1-4 每个程序都有前缀，默认前缀与提示符相同


## 3.2 下载过程
```
TFTP from server 192.168.1.251; our IP address is 192.168.1.6
Filename 'Image-TR600-Plus'.
Load address: 0x40400000
Loading: T #########
```
  - 服务器IP __192.168.1.251__
  - 本机IP __192.168.1.6__
  - 下载文件名 __Image-TR600-Plus__
  - 尝试与服务器通信 __T__
  - 正常通信 __#__


# 4 修改服务器IP与本机IP
  由于生产车间IP限制，必须将修改指定IP

## 4.1 处理方式
  - 按  <font color=red>__'2'，'Enter'__ </font> 退出菜单模式

```
[d] 调试uboot
[k] 调试内核
[ ] TODO 校验内核
[s] 配置 
[r] 重启 
[q] 退出 
TR600-Plus > q
```
  - 弹出命令模式提示符
```
MX28 U-Boot > 
```  
  - 可以查看当前配置 <font color=red>____print__</font>

```
MX28 U-Boot > print
bootcmd=run nand_boot
bootdelay=1
baudrate=115200
ipaddr=192.168.1.6
serverip=192.168.1.251
netmask=255.255.255.0
bootfile="uImage"
*****************************
    省略
*****************************
MX28 U-Boot > 
```
  - 修改服务器IP和本机IP <font color=red>__setenv __</font>

```
MX28 U-Boot > setenv serverip 192.168.1.1
MX28 U-Boot > setenv ipaddr 192.168.1.5
```
  - 查看配置内容 <font color=red>__print__</font>

```
MX28 U-Boot > print
*****************************
    省略
*****************************
serverip=192.168.1.1
ipaddr=192.168.1.5

Environment size: 2679/131068 bytes
```

  - 保存配置 <font color=red>__saveenv__</font>

```
MX28 U-Boot > saveenv
Saving Environment to NAND...
Erasing Nand...
NAND Erasing at 0x0000000000100000 -- 100% complete.
Writing to Nand... done
```


# 5 TFTP下载重试
```
TFTP from server 192.168.1.251; our IP address is 192.168.1.6
Filename 'TR600-Plus-logo.bmp'.
Load address: 0x40400000
Loading: T T T T T T T
```

## 5.1 处理方式
  - 检查服务器IP 是否存在，这里服务器IP是 192.168.1.251
  - 检查本机IP是否有冲突，默认本机IP 192.168.1.6
  - 检查服务器是否开启TFTP，

# 6 TFTP找不到文件

```
TFTP from server 192.168.1.251; our IP address is 192.168.1.6
Filename 'TR600-Plus-logo.bmp'.
Load address: 0x40400000
Loading: T 
TFTP error: 'File not found' (1)
Starting again
```
  以上信息表明已经与服务器连接成功，但是没有在服务器上找到 __TR600-Plus-logo.bmp__
## 6.1 处理方式
  - 在服务器TFTP目录拷贝相应文件
  - 检查TFTP服务器共享目录是否正确  
  ![](./images/tftp-4.png)

## 6.2 TFTP下载到一半进行不下去

```
TFTP from server 192.168.1.251; our IP address is 192.168.1.6
Filename 'TR600-Plus-uImage'.
Load address: 0x40400000
Loading: T ###########################T T T T T T T T 
```
  现象表明服务器正常


## 6.3 处理方式
  - 检查网线、交换机连接状态
  - 极有可 IP 冲突，内网存在 192.168.1.6


