MX287下载方法
=============

# 1 需要下载的内容
  - USB下载引导（uboot）
  - TFTP 一键下载如下内容
    - TFTP下载内核（uImage）
    - TFTP下载文件系统（rootfs）
    - TFTP下载logo
  - [U盘/SD卡烧写定制产品](http://g-link:3000/GlinkLib/bbs-TR600-MX287/src/doc/pcb-test/test-update.md)
    （必须完成如上所有步骤后才能执行）


## 1.1 最新的下载镜像文件在这里
  - 打开以日期命名的目录 [地址](./) 


## 1.2 所有用到的软件下载

 - [USB下载工具MfgTool](./MfgTool-1.6.2.055.rar)  
 ![](./images/howo-0-ico.png)
 - [TFTPd (32bit)](./Tftpd32-4.52-setup.exe)
 - [TFTPd (64bit)](./Tftpd64-4.60-setup.exe)  
 ![](./images/howo-3-ico.png)
 - [SecureCRT](./SecureCRT.rar)  
 ![](./images/howo-2-ico.png)

  

## 1.3 环境配置
  - [配置MfgTool环境](./env-MfgTool.md)
  - [配置TFTP环境](./env-tftp.md)
  - [配置SecureCRT](./env-SecureCRT.md)

## 1.4 常见问题处理方式
  - 参考 [《下载常见问题解释与处理》](./detail.md)
  
<br><br><br><br>
# 2 命名
  研发给予的所有下载文件均包含版本号，下载前先 __去掉版本号__ ，具体版本号根据情况而定



<table>
  <tr>
    <td><b>原始版本控制</b></td>
    <td><b>通用版本</b></td>
    <td><b>专用版本</b></td>
  </tr>
  <tr>
    <td>imx28_ivt_uboot-1.0-rc1</td>
    <td>imx28_ivt_uboot.sb</td>
    <td>imx28_ivt_uboot.sb</td>
  </tr>
  <tr>
    <td>uImage-v1.1-rc2</td>
    <td>uImage</td>
    <td>TR600-Plus-uImage</td>
  </tr>
  <tr>
    <td>logo-glink-5.6.bmp</td>
    <td>logo.bmp</td>
    <td>TR600-Plus-logo.bmp</td>
  </tr>
  <tr>
    <td>rootfs-v1.0-rc2.ubifs</td>
    <td>rootfs.ubifs</td>
    <td>TR600-Plus-rootfs.ubifs</td>
  </tr>
</table>


# 3 USB下载引导（uboot）
## 3.1 硬件连接
  - 连接串口
  - USB OTG口连接电脑
  - 供电12V  
  ![](./images/connect-1.jpg)
  - <font color=red>__跳线帽短接 WDG、USB__（重要）</font>   
  ![](./images/connect-2.jpg)
  


## 3.2 执行下载
  - 启动PC端下载程序  
<font color=red>__注意__</font>  
  不同产品使用点击不同的按钮，否则后面的下载过程会失败  
  ![](./images/howo-0-ico.png)
  ![](./images/usb-download-1.png)

  - options->configuration  
  ![](./images/usb-download-2.png)

  - 确定连接状态  
  ![](./images/detail-2.png)   

  - 选择只下载uboot（NAND uboot only）  
  ![](./images/usb-download-3.png)

  - 选择开始等待下载  
  ![](./images/usb-download-4.png)

  - 下载成功
  ![](./images/usb-download-5.png)  
  如果连接串口可看到如下提示

```
Finished pass 1 successfully
UTP: sending Success
UTP: received command 'send'
UTP: sending Success
UTP: received command '$ kobs-ng init $FILE'
UTP: sending Busy
UTP: executing "kobs-ng init $FILE"
UTP: sending Success
UTP: received command '$ echo Update Complete!'
UTP: sending Busy
UTP: executing "echo Update Complete!"
Update Complete!
UTP: sending Success
g_file_storage gadget: high speed config #1
```

## 3.3 收尾工作
 - 断开USB跳线帽
 - 点击PC端下载工具的“停止”按钮，否则会在下载一次



<br><br><br><br>
# 4 TFTP下载教程精简版
  - 上电长按 'Enter'
  - '2' 'Enter' __TFTP一键下载__(代替下面三个步骤)
    - '2' 'Enter' __TFTP下载内核（uImage）__
    - '3' 'Enter' __TFTP下载文件系统（rootfs）__
    - '4' 'Enter' __TFTP下载logo__

  如下是详细步骤与细节

<br><br><br><br>



# 5 TFTP一键下载

## 5.1 硬件连接
  - 连接串口
  - 连接以太口
  - 供电12V  
  ![](./images/connect-4.jpg)
  - <font color=red> __跳线帽只短接 WDG__（重要）  </font>   
  ![](./images/connect-5.jpg)

## 5.2 执行下载
  - 启动TFTP服务器  
  ![](./images/howo-3-ico.png)  
  - 启动SecureCRT  
  ![](./images/howo-2-ico.png)
  - PC IP 192.168.1.251（默认）

  - 上电后在SecureCRT按 ‘Enter’（一直按着）  

  弹出如下提示
```
SHA1 7b5f7ee3e0b7706e90ec7e520c5933602d602009
##### Glink [uboot] Apr 18 2017 11:45:52#####
```
 <font color=red>
```
[a] 一键下载下面4项
[1] 专用版本 下载uboot    TR600-Plus-uboot.bin
[2] 专用版本 下载kernel   TR600-Plus-uImage
[3] 专用版本 下载rootfs   TR600-Plus-rootfs.ubifs
[4] 专用版本 下载logo     TR600-Plus-logo.bmp
[L] 定制产品logo
```
</font>
```
-----------------------------------
[5] 通用版本 下载uboot
[6] 通用版本 下载kernel
[7] 通用版本 下载rootfs
[8] 通用版本 下载logo
-----------------------------------
[bn] 从Nand启动
[f] 格式化分区
[d] 调试uboot
[k] 调试内核
[ ] TODO 校验内核
[s] 配置 
[r] 重启 
[q] 退出 
TR600-Plus >
```
 




  - 按  <font color=red>__'a'，'Enter'__ </font> 执行 （如果超过2min未任何操作则重启）  
    

  - 自动下载uImage、rootfs、logo  

### 5.2.1 下载完成标志

  当看三组 __Loading: T #####__ 并在最后看到 __TR600-Plus >__ 表示烧录完成，
  可以关电源

```
Loading: T #################################################################
         #################################################################
         ###############################################################
***********************
   省略若干行
***********************




Loading: T #################################################################
         #################################################################
         ###############################################################
         ###############################################################

----------------------等待2min------------------

***********************
   省略若干行
***********************


Loading: T ####################
***********************
   省略若干行
***********************



TR600-Plus >

```


<br><br><br>
  如遇到问题进行不下去，参考 [《下载常见问题解释与处理》](./detail.md)

  如上几个文件可单独下载，下载方法如下



<br><br><br><br>
# 6 TFTP下载内核（uImage）

## 6.1 硬件连接
  - 与 __TFTP一键下载__ 完全相同
## 6.2 执行下载
  - 按  <font color=red>__'2'，'Enter'__</font> 执行 （如果超过2min未任何操作则重启）  




<br><br><br><br>
# 7 TFTP下载文件系统（rootfs）
## 7.1 硬件连接
  - 与 __TFTP一键下载__ 完全相同
## 7.2 执行下载
  - 按  <font color=red>__'3'，'Enter'__</font> 执行 （如果超过2min未任何操作则重启）  
  - 大约等待2min



<br><br><br><br>
# 8 TFTP下载logo
## 8.1 硬件连接
  - 与 __TFTP一键下载__ 完全相同 


## 8.2 执行下载
  - 按  <font color=red>__'4'，'Enter'__</font> 执行 （如果超过2min未任何操作则重启）  

<br><br><br><br>
## 8.3 定制Logo
  - 按  <font color=red>__'L'，'Enter'__</font> 寻找是否有支持的LOGO厂家

```
[1] glink
[2] 南京华拓
[3] 安捷伦
[4] TI
[n] 其他：
TR600-Plus > 2
```

  - 若没有则<font color=red>__'n'，'Enter'__</font>
  - 输入 <font color=red>__具体的logo名称，'Enter'__</font>  
    （名称位置对于TFTP服务器D:/TFTP目录下）

```
[1] glink
[2] 华拓
[3] 安捷伦
[4] TI
[n] 其他：
TR600-Plus >n
输入名称:logo-5.6-ABCD.bmp

```

## 9 核心板序列号设置
  参考文档 [《核心板序列号设置》](./mx287-board-sn.md)