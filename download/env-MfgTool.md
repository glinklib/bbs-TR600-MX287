配置MfgTool环境
===============

# 配置TR600-Plus环境
  - 解压MfgTool-1.6.2.055.rar到 __D:\MX287__ 目录
  - 重命名MfgTool-1.6.2.055为 __TR600-Plus__
  - 进入目录D:\MX287\TR600-Plus
      - 给MfgTool.exe 创建快捷方式，并重命名为 TR600-Plus
      - 剪切 TR600-Plus 快捷方式到桌面
  - uboot程序存放位置在  
    __D:\MX287\TR600-Plus\Profiles\MX28 Linux Update\OS Firmware\files\__  
    名称为  
    __imx28_ivt_uboot.sb__


# 配置其他产品环境（以TF500为例）
  - 复制 __D:\MX287\TR600-Plus__ 为 __D:\MX287\TF500__
  - 去掉  
    __D:\MX287\TF500\Profiles\MX28 Linux Update\OS Firmware\files__  
    目录下的  
    __imx28_ivt_uboot.sb__  
    <font color=red>用TF500的uboot覆盖它（不够现在还么有该版本）</font>
  - 进入目录 __D:\MX287\TF500__
      - 给MfgTool.exe 创建快捷方式，并重命名为 __TF500__
      - 剪切 TF500 快捷方式到桌面

  其他产品环境如法炮制  
  ![](./images/howo-0-ico.png)
  ![](./images/env-mfgtool.png)
# 原因
  复制多个MfgTool环境是因为MfgTool烧录方式不提供选择烧录文件，  
  为了避免实际生产中不同产品都去覆盖 __imx28_ivt_uboot.sb__ 文件
  而照成版本不可控。
