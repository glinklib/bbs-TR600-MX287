核心板序列号设置
=============
> 2017-6-22  
> 吴梦龙

# 1 Uboot状态下修改

## 1.1 查阅SN

  默认启动后序列号 __"SN"__ 是 "?????????????"

  ![](./images/uboot-sn-1.png)

## 1.2 修改
  - 输入 __小写"s"__
  - 输入 具体的序列号如：__"tr600-20170622-001"__
  - 输入 __回车__ 保存
  
  ![](./images/uboot-sn-2.png)


## 1.3 结果
  现在序列号以及变了

  ![](./images/uboot-sn-3.png)

# 2 使用
  在Linux环境下可以查阅Uboot给予配置信息，所有内容均在
  __/sys/class/misc/product/__ 下，所有属性均是__只读__

  查询 __序列号__

```
[root@TR700-OTDR[TR700_101_V3] ~]# cat /sys/class/misc/product/sn 
tr600-20170622-001
```

  查询 __板卡名__

```
[root@TR700-OTDR[TR700_101_V3] ~]# cat /sys/class/misc/product/board 
TR700-OTDR[TR700_101_V3]
```

  uboot传递给Linux的详细参数

```
[root@TR700-OTDR[TR700_101_V3] ~]# cat /proc/cmdline 
gpmi=g console=ttyAM0,115200n8  
ubi.mtd=5 root=ubi0:rootfs rootfstype=ubifs fec_mac= ethact mem=128M lpj=1130496  
BOARD=TR700-OTDR[TR700_101_V3] 
BOOT_SHA1=1fcfb63a6c7a37274951d9d6f12ea775a8f79c32 
BOOT_BUILD="Feb 10 2018_09:08:11" 
SN=tr600-20170622-001
```
