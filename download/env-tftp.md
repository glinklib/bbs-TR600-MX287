配置TFTP环境
==========================
  - 解压 __Tftpd32-4.52-setup.exe__ 或 __Tftpd64-4.60-setup.exe__
  - 所有要下载的文件均放入d:/tftp目录（有几个项目就拷几个）  
  ![](./images/tftp-2.png)
  - 启动TFTP服务器  
  ![](./images/howo-3-ico.png)  
  ![](./images/tftp-1.png)
  - 点击Setting配置
    - 只开启 TFTP Server  
  ![](./images/tftp-3.png)  
    - 选择目录，和开放的端口  
  ![](./images/tftp-4.png)

  配置完毕，下次打开就不必要再做配置
